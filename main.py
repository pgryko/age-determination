import datetime
import calendar
import click
from datetime import date


@click.command()
@click.option('--year', prompt='Year of Birth',type=int)
@click.option('--month', prompt='Month of Birth',type=int)
@click.option('--day', prompt='Day of birth',type=int)
def get_user_info(year, month, day):
    """
    This function calls python input and asks user to provide date of birth
    in the format of (year, month, day)

    :return:

    String (year, month, day)
    """


    return year, month, day


def bd_happened(dateNow, birthDayDate):
    """ A function that returns a boolean, on whether a date has passed this year

    :param dateNow:
    :param birthDayDate:
    :return: Bool
    """

    day_age = dateNow[2] - birthDayDate[2]
    month_age = dateNow[1] - birthDayDate[1]

    if month_age < 0:
        return False
    if month_age == 0:
        if day_age < 0:
            return False

    return True

def get_num_days_in_month(year, month):
    return calendar.monthrange(year, month)

def calc_time(user_birthdate):

    date_now =[date.today().year, date.today().month, date.today().day]
    print(date_now[0] - user_birthdate[0], date_now[1] - user_birthdate[1], date_now[2] - user_birthdate[2])

    year_age = date_now[0] - user_birthdate[0]
    month_age = 12 - user_birthdate[1] + date_now[1]

    last_month = 12 if date_now[1] == 1 else date_now[1] - 1
    last_year = date_now[0] - 1 if date_now[1] == 1 else date_now[0]
    last_month_days = get_num_days_in_month(last_year, last_month)
    day_age = last_month_days[1] - user_birthdate[2] + date_now[2]
    if bd_happened(date_now, user_birthdate):
        year_age -= 1
    if bd_happened(date_now, user_birthdate):
        month_age -= 1
    print ("Your age is ", (year_age, month_age, day_age))


if __name__ == "__main__":
    date_now = [2018, 1, 17]
    birth_date = [1980, 11, 12]

    bd_happened(date_now, birth_date)
#     user_input = get_user_info()
#
#     calc_time(user_input)
