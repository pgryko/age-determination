# This is a demo of how to clean up a project

## Intro

A lot of developer time, is spent trying to debug and maintain code.
We generally struggle with complexity i.e. Keeping things simple.

There are 4 approaches that you should use for cleaning up code:

- [ ] The python debugger
- [ ] Unit tests
- [ ] Documentation
- [ ] Breaking things down into smaller, testable functions

This project is not designed to show 'good code' - rather to demo how
to improve existing code.
